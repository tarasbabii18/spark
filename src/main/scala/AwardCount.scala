import org.apache.spark.sql.SparkSession

object AwardCount {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("AwardCount")
      .getOrCreate
    val tableWithFullNames = spark
      .read.textFile("C:\\spark\\workspace\\spark\\src\\resources\\Master.csv").rdd
    val tableWithAwards = spark
      .read.textFile("C:\\spark\\workspace\\spark\\src\\resources\\AwardsPlayers.csv").rdd

    val playersByFullName = tableWithFullNames.map(line =>
      (line.split(",")(0),
        line.split(",")(3) + " " +
          line.split(",")(4)))

    val playersByAward = tableWithAwards.map(line =>
      line.split(",")(0))
      .map(playerId => (playerId, 1))
      .reduceByKey(_+_)

    val joinedTablesResult = playersByFullName
      .join(playersByAward)
      .sortBy(-_._2._2)
      .map(line => line._2._1 + " - " + line._2._2)
      .saveAsTextFile("D:\\result.csv")

    spark.stop()
  }
}
